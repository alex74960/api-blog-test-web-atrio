const express = require('express');
const mysql = require('mysql')
const poolBLOG = mysql.createPool({
    connectionLimit: 500,
    debug: false,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
})

// Nous définissons ici les paramètres du serveur.
const hostname = 'localhost';
const port = 3000;
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// Démarrer le serveur 
app.listen(port, hostname, function () {
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});

//Implémentation de GET, POST, PUT et DELETE
// GET
app.get('/api/posts', (req, res) => {
    const posts = getPost()
    res.json({ posts });
})

//POST
app.post('/api/post', (req, res) => {
    const title = "lorem ipsum"
    const content = "lorem ipsum"
    const user_id = 5
    instertPost(title, content, user_id)
})

//PUT
app.put('/api/post/:id', (req, res) => {
    //Fonctionnalité non implémentée
    res.json({ message: "Mise à jour d’un article", methode: req.method });
})

//DELETE
app.delete('/api/post/delete/:id', (req, res) => {
    //Fonctionnalité non implémentée
    res.json({ message: "Suppression d’un article", methode: req.method });
})

function instertPost(title, content, user_id) {
    return new Promise(function (resolve, reject) {
        const sql = "INSERT INTO posts (id, title, content, created_date, user_id) VALUES (NULL, '" + title + "', '" + content + "', NOW(), '" + user_id + "')"
        poolBLOG.getConnection(function (error, connection) {
            if (error) {
                console.error(error)
                reject(new Error('Erreur lors de l\'enregistrement : impossible de se connecter à la base de données'))
            }
            connection.query(sql, function (error, result) {
                if (error) {
                    console.error(error)
                    reject(new Error('Une erreur est survenue lors de l\'insertion du post en base de données'))
                }
                try {
                    resolve(result.insertId)
                } catch (error) {
                    console.error(error)
                    reject(new Error('Une erreur est survenue lors de l\'insertion du post en base de données'))
                }
            })
            connection.on('error', function (error) {
                console.error(error)
                reject(new Error('Une erreur est survenue lors de la connexion à la base de données'))
            })
        })
    })
}

function editPost(id) {
    //
}

function deletePost(id) {
    //
}

function getPost() {
    try {
        return new Promise(function (resolve, reject) {
            poolBLOG.getConnection(function (error, connection) {
                if (error) {
                    console.error(error)
                    reject(new Error('Erreur lors de l\'enregistrement : impossible de se connecter à la base de données'))
                } else {
                    connection.query(
                        "SELECT * FROM posts",
                        function (error, results) {
                            connection.release()
                            if (error) {
                                console.error(error)
                                reject(new Error('Une erreur est survenue lors de la recherche du post en base de données'))
                            }
                            if (results !== null && results[0] !== null && results.length > 0) {
                                try {
                                    resolve(results[0])
                                } catch (error) {
                                    console.error(error)
                                    reject(new Error('Le post n° ' + id + ' n\'a pas été trouvé en base'))
                                }
                            } else {
                                reject(new Error('Le post n° ' + id + ' n\'a pas été trouvé en base'))
                            }
                        }
                    )
                    connection.on('error', function (error) {
                        console.error(error)
                        reject(new Error('Une erreur est survenue lors de la connexion à la base de données'))
                    })
                }
            })
        })
    } catch (error) {
        throwError({ 'detail': 'Une erreur est survenue lors de la sauvegarde en base de données' })
    }
}

function throwError(error = new Error(500)) {
    throw Object.assign(new Error(), {
        status: error.status || 500,
        code: error.code || 'EINTERNAL',
        title: error.title || 'Internal Server Error',
        detail: error.detail || 'Erreur interne.',
    }, error)
}